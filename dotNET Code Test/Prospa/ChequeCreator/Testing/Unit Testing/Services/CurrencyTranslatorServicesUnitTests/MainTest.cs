﻿using NUnit.Framework;
using Prospa.ChequeCreator.Services.CurrencyTranslatorServices;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorServices;
using Should;

namespace Prospa.ChequeCreator.Testing.UnitTesting.Services.CurrencyTranslatorServicesUnitTests
{
    [TestFixture]
    public class MainTest
    {
        private ICurrencyTranslatorServices _currencyTranslatorServices;
        private const string DenominationSingle = "cent";
        private const string DenominationPlural = "cents";

        [TestFixtureSetUp]
        public void Start()
        {
            _currencyTranslatorServices = new CurrencyTranslatorServices();
        }

        [Test]
        public void PluralizeValueOfZero()
        {
            const int value = 0;
            var result = _currencyTranslatorServices.PluralizeDenomination(DenominationSingle, value);
            result.ShouldEqual(DenominationPlural);
        }

        [Test]
        public void PluralizeValueOfOne()
        {
            const int value = 1;
            var result = _currencyTranslatorServices.PluralizeDenomination(DenominationSingle, value);
            result.ShouldEqual(DenominationSingle);
        }

        [Test]
        public void PluralizeValueOfTwo()
        {
            const int value = 2;
            var result = _currencyTranslatorServices.PluralizeDenomination(DenominationSingle, value);
            result.ShouldEqual(DenominationPlural);
        }

        [Test]
        public void PluralizeValueOfMoreThanTwo()
        {
            const int value = 5678;
            var result = _currencyTranslatorServices.PluralizeDenomination(DenominationSingle, value);
            result.ShouldEqual(DenominationPlural);
        }

        [Test]
        public void PluraliseSingleOctopus()
        {
            const int value = 1;
            var result = _currencyTranslatorServices.PluralizeDenomination("octopus", value);
            result.ShouldEqual("octopus");
        }

        [Test]
        public void PluraliseMultipleOctopi()
        {
            const int value = 2;
            var result = _currencyTranslatorServices.PluralizeDenomination("octopus", value);
            result.ShouldEqual("octopi");
        }

        [Test]
        public void GetWordsForIntegerValueOf1()
        {
            const int value = 1;
            var result = _currencyTranslatorServices.GetIntegerAsWords(value);
            result.ShouldEqual("one");
        }

        [Test]
        public void GetWordsForIntegerValueOf13()
        {
            const int value = 13;
            var result = _currencyTranslatorServices.GetIntegerAsWords(value);
            result.ShouldEqual("thirteen");
        }

        [Test]
        public void GetWordsForIntegerValueOf45()
        {
            const int value = 45;
            var result = _currencyTranslatorServices.GetIntegerAsWords(value);
            result.ShouldEqual("forty-five");
        }

        [Test]
        public void GetWordsForIntegerValueOf239()
        {
            const int value = 239;
            var result = _currencyTranslatorServices.GetIntegerAsWords(value);
            result.ShouldEqual("two hundred and thirty-nine");
        }

        [Test]
        public void GetWordsForIntegerValueOf5678()
        {
            const int value = 5678;
            var result = _currencyTranslatorServices.GetIntegerAsWords(value);
            result.ShouldEqual("five thousand six hundred and seventy-eight");
        }

        [Test]
        public void GetCentsFrom23Pt45()
        {
            const decimal amount = 23.45m;
            var result = _currencyTranslatorServices.GetCents(amount);
            result.ShouldEqual(45);
        }

        [Test]
        public void GetCentsFrom23Pt49()
        {
            const decimal amount = 23.49m;
            var result = _currencyTranslatorServices.GetCents(amount);
            result.ShouldEqual(49);
        }

        [Test]
        public void GetCentsFrom23Pt99()
        {
            const decimal amount = 23.99m;
            var result = _currencyTranslatorServices.GetCents(amount);
            result.ShouldEqual(99);
        }

        [Test]
        public void GetCentsFrom23Pt0()
        {
            const decimal amount = 23.0m;
            var result = _currencyTranslatorServices.GetCents(amount);
            result.ShouldEqual(0);
        }

        [Test]
        public void GetDollarsFrom0Pt0()
        {
            const decimal amount = 0.0m;
            var result = _currencyTranslatorServices.GetDollars(amount);
            result.ShouldEqual(0);
        }

        [Test]
        public void GetDollarsFrom0Pt45()
        {
            const decimal amount = 0.45m;
            var result = _currencyTranslatorServices.GetDollars(amount);
            result.ShouldEqual(0);
        }

        [Test]
        public void GetDollarsFrom23Pt0()
        {
            const decimal amount = 23.0m;
            var result = _currencyTranslatorServices.GetDollars(amount);
            result.ShouldEqual(23);
        }

        [Test]
        public void GetDollarsFrom23Pt45()
        {
            const decimal amount = 23.45m;
            var result = _currencyTranslatorServices.GetDollars(amount);
            result.ShouldEqual(23);
        }

        [Test]
        public void GetDollarsFrom23Pt49()
        {
            const decimal amount = 23.49m;
            var result = _currencyTranslatorServices.GetDollars(amount);
            result.ShouldEqual(23);
        }

        [Test]
        public void GetDollarsFrom23Pt54()
        {
            const decimal amount = 23.54m;
            var result = _currencyTranslatorServices.GetDollars(amount);
            result.ShouldEqual(23);
        }

        [Test]
        public void CapitaliseFirst_MixedCase()
        {
            const string amount = "this Is a MiXed amounT";
            var result = _currencyTranslatorServices.CapitalizeAmount(amount);
            result.ShouldEqual("This is a mixed amount");
        }

        [Test]
        public void CapitaliseFirst_LowerCase()
        {
            const string amount = "this is a mixed amount";
            var result = _currencyTranslatorServices.CapitalizeAmount(amount);
            result.ShouldEqual("This is a mixed amount");
        }

        [Test]
        public void CapitaliseFirst_UpperCase()
        {
            const string amount = "THIS IS A MIXED AMOUNT";
            var result = _currencyTranslatorServices.CapitalizeAmount(amount);
            result.ShouldEqual("This is a mixed amount");
        }

    }
}
