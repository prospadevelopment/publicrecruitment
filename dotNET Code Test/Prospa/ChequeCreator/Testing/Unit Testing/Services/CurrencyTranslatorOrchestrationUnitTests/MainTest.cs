﻿using Moq;
using NUnit.Framework;
using Prospa.ChequeCreator.Services.CurrencyTranslatorOrchestration;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorOrchestration;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorServices;
using Should;

namespace Prospa.ChequeCreator.Testing.UnitTesting.Services.CurrencyTranslatorOrchestrationUnitTests
{
    [TestFixture]
    public class MainTest
    {

        private ICurrencyTranslatorOrchestration _currencyTranslatorOrchestration;
        private Mock<ICurrencyTranslatorServices> _numberTranslatorServicesMock;

        [TestFixtureSetUp]
        public void Start()
        {
            _numberTranslatorServicesMock = new Mock<ICurrencyTranslatorServices>();

            _numberTranslatorServicesMock
                .Setup(
                    f => f.CapitalizeAmount("thirty-four dollars and sixty-five cents")
                )
                .Returns("Thirty-four dollars and sixty-five cents");

            _numberTranslatorServicesMock
                .Setup(
                    f => f.GetCents(34.65m)
                )
                .Returns(65);

            _numberTranslatorServicesMock
                .Setup(
                    f => f.GetDollars(34.65m)
                )
                .Returns(34);

            _numberTranslatorServicesMock
                .Setup(
                    f => f.GetIntegerAsWords(34)
                )
                .Returns("thirty-four");

            _numberTranslatorServicesMock
                .Setup(
                    f => f.GetIntegerAsWords(65)
                )
                .Returns("sixty-five");

            _numberTranslatorServicesMock
                .Setup(
                    f => f.PluralizeDenomination("dollar", 34)
                )
                .Returns("dollars");

            _numberTranslatorServicesMock
                .Setup(
                    f => f.PluralizeDenomination("cent", 65)
                )
                .Returns("cents");


            _currencyTranslatorOrchestration = new CurrencyTranslatorOrchestration(_numberTranslatorServicesMock.Object);
        }

        [Test]
        public void HappyPath()
        {
            var amount = 34.65m;
            var result = _currencyTranslatorOrchestration.GetAmountInWords(amount);

            _numberTranslatorServicesMock
                .Verify(
                    f => f.CapitalizeAmount("thirty-four dollars and sixty-five cents"),
                    Times.Once
                );

            _numberTranslatorServicesMock
                .Verify(
                    f => f.GetCents(amount),
                    Times.Once
                );

            _numberTranslatorServicesMock
                .Verify(
                    f => f.GetDollars(amount),
                    Times.Once
                );

            _numberTranslatorServicesMock
                .Verify(
                    f => f.GetIntegerAsWords(34),
                    Times.Once
                );

            _numberTranslatorServicesMock
                .Verify(
                    f => f.GetIntegerAsWords(65),
                    Times.Once
                );

            _numberTranslatorServicesMock
                .Verify(
                    f => f.PluralizeDenomination("dollar", 34),
                    Times.Once
                );

            _numberTranslatorServicesMock
                .Verify(
                    f => f.PluralizeDenomination("cent", 65),
                    Times.Once
                );

            result.ShouldEqual("Thirty-four dollars and sixty-five cents");
        }


    }
}
