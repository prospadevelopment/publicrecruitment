﻿namespace Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorOrchestration
{
    public interface ICurrencyTranslatorOrchestration
    {
        string GetAmountInWords(decimal amount);
    }
}
