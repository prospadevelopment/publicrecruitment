﻿namespace Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorServices
{
    public interface ICurrencyTranslatorServices
    {
        string CapitalizeAmount(string amount);
        int GetDollars(decimal amount);
        int GetCents(decimal amount);
        string PluralizeDenomination(string denomination, int value);
        string GetIntegerAsWords(int value);
    }
}
