﻿using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorOrchestration;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorServices;

namespace Prospa.ChequeCreator.Services.CurrencyTranslatorOrchestration
{
    public class CurrencyTranslatorOrchestration : ICurrencyTranslatorOrchestration
    {
        private readonly ICurrencyTranslatorServices _currencyTranslatorServices;

        public CurrencyTranslatorOrchestration(ICurrencyTranslatorServices currencyTranslatorServices)
        {
            _currencyTranslatorServices = currencyTranslatorServices;
        }

        public string GetAmountInWords(decimal amount)
        {
            var dollars = _currencyTranslatorServices.GetDollars(amount);
            var cents = _currencyTranslatorServices.GetCents(amount);

            var result = string.Format(
                    "{0} {1} and {2} {3}",
                    _currencyTranslatorServices.GetIntegerAsWords(dollars),
                    _currencyTranslatorServices.PluralizeDenomination("dollar", dollars),
                    _currencyTranslatorServices.GetIntegerAsWords(cents),
                    _currencyTranslatorServices.PluralizeDenomination("cent", cents)
                );

            return _currencyTranslatorServices.CapitalizeAmount(result);
        }
    }
}
