﻿using Microsoft.Owin;
using Owin;
using Prospa.ChequeCreator.Presentation.Web;

[assembly: OwinStartup(typeof(Startup))]
namespace Prospa.ChequeCreator.Presentation.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
