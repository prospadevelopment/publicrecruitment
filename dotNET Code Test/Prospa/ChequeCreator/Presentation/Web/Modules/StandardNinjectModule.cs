﻿using Ninject.Modules;
using Prospa.ChequeCreator.Services.CurrencyTranslatorOrchestration;
using Prospa.ChequeCreator.Services.CurrencyTranslatorServices;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorOrchestration;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorServices;

namespace Prospa.ChequeCreator.Presentation.Web.Modules
{
    public class StandardNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICurrencyTranslatorServices>().To<CurrencyTranslatorServices>();
            Bind<ICurrencyTranslatorOrchestration>().To<CurrencyTranslatorOrchestration>();
        }
    }
}